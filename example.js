const AirtableDump = require('./index');

const airtable = new AirtableDump({
    apiKey: 'keyXXXXXXXXXXXXXX',
    baseId: 'appXXXXXXXXXXXXXX',
});

/*
    The example base on Airtable has a table called 'Stores' and another called 'Units'
    The Stores table has a Name field, which operates as the row ID, and optional Notes field
    The Products table has an SKU field (ID), and an 'In stock' field which links to one or more Stores
*/

airtable.addTable('Stores', 'Name', ['Name', 'Notes'], [
    { Name: 'Store 1' },
    { Name: 'Store 2', Notes: 'This store is the best one' },
    { Name: 'Store 3' },
    { Name: 'Store 4' },
]);

airtable.addTable('Products', 'SKU', ['SKU', 'In stock']);
airtable.addTableData(
    'Products',
    [
        { SKU: '001', _join: { 'In stock': { table: 'Stores', ids: ['Store 2', 'Store 4'] } } },
        { SKU: '002', _join: { 'In stock': { table: 'Stores', ids: ['Store 2', 'Store 1'] } } },
        { SKU: '003', _join: { 'In stock': { table: 'Stores', ids: ['Store 3', 'Store 4'] } } },
        { SKU: '004', _join: { 'In stock': { table: 'Stores', ids: ['Store 4', 'Store 2','Store 1'] } } },
        { SKU: '005', _join: { 'In stock': { table: 'Stores', ids: ['Store 2', 'Store 3'] } } },
        { SKU: '006', _join: { 'In stock': { table: 'Stores', ids: ['Store 2', 'Store 4','Store 3'] } } },
        { SKU: '007', _join: { 'In stock': { table: 'Stores', ids: ['Store 1', 'Store 4'] } } },
        { SKU: '008', _join: { 'In stock': { table: 'Stores', ids: ['Store 3', 'Store 2'] } } },
        { SKU: '009', _join: { 'In stock': { table: 'Stores', ids: ['Store 2', 'Store 4'] } } },
    ]
);

airtable.dump()
    .then(() => {
        console.log('All done!');
    })
    .catch((e) => {
        console.error(e);
    });

