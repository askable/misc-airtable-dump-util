const Airtable = require('airtable');
const _ = require('lodash');

class DumpError extends Error {
    constructor(message, attachments = []) {
        super(message);
        this.name = "DumpError";
        this.attachments = attachments;
    }
}

class AirtableDump {
    constructor ({
        apiKey,
        baseId,
        tables = [],
        rateLimit = 4, /* (requests per second) */
        recordLimit = 8, /* (records per request) */
        requestDelay = 1100 /* (time between request groups) */,
        ...options
    } = {}) {
        const browser = new Airtable({ apiKey });
        this.base = browser.base(baseId);

        this.idMap = {};
        this.joins = [];

        this.tables = tables;

        this.logFunction = options.logFunction || console.log;
        this.log = '';
        
        this.rateLimit = rateLimit 
        this.recordLimit = recordLimit 
        this.requestDelay = requestDelay;
        
        this.recordsPerSecond = this.recordLimit * this.rateLimit;
    }

    timeLog (...stuff) {
        if (!this._startTime) {
            this._startTime = Date.now();
        }

        const time = `${((Date.now() - this._startTime) / 1000).toFixed(2)}s`;
        this.logFunction(time, ...stuff);
    
        if (this.log) {
            this.log += '\n';
        }
    
        this.log += [
            time,
            ...stuff.map(thing => JSON.stringify(thing, undefined, 2))
        ].join(' ');
    }

    wait (ms) {
        return new Promise((resolve) => {
            setTimeout(resolve, ms);
        });
    }

    async withRety (fn, maxAttempts = 5, attempt = 1) {
        try {
            return await fn();
        } catch (error) {
            if (attempt >= maxAttempts) {
                console.error(`Function failed with message: ${error.message} on final attempt (${attempt})`);
                throw error;
            }
            console.error(`Function failed with message: ${error.message} on attempt ${attempt}, retrying...`);
            await this.wait(2000);
            return this.withRety(fn, maxAttempts, attempt + 1);
        }
    }

    addTable(name, idField, fields, data) {
        this.tables.push({ name, _id: idField, fields, data: data || undefined });
    }

    addTableData(tableName, data) {
        const table = _.find(this.tables, { name: tableName });
        if (!table) throw new Error(`Table ${tableName} not found`);

        if (table.data && table.data.length) {
            table.data.push(...data);
        } else {
            table.data = data;
        }
    }

    async airtableRateLimit(op, records) {
        if (records.length > this.recordsPerSecond) throw new Error('More than 50 records at a time may hit rate limit');
        const promises = [];
        _.chunk(records, this.recordLimit).forEach(chunk => {
            promises.push(this.withRety(() => op(chunk)).catch((error) => { throw { chunk, error }; }));
        });
        promises.push(this.wait(this.requestDelay));
    
        const result = await Promise.all(promises)
            .catch((e) => {
                console.error('airtableRateLimit failed at chunk:');
                console.error(e.chunk);
                throw new DumpError(e.error, [{ text: [`*airtableRateLimit failed at chunk:*`, '```', JSON.stringify(e.chunk, undefined, 2), '```'].join('\n') }]);
            });
    
        return _.chain(result).flatten().filter().value();
    }
    
    async mapTableIds() {
        for (let i = 0; i < this.tables.length; i += 1) {
            this.timeLog(`Mapping ${this.tables[i].name}`);
            await this.mapEachTableIds(this.tables[i]).catch((e) => {
                console.error(e);
            });
        }
    
        this.timeLog('Finished mapping IDs', _.mapValues(this.idMap, ids => `${_.values(ids).length - 1} (${ids._missing.length} missing)`));
    }

    mapEachTableIds(table) {
        return new Promise((resolve, reject) => {
            // add _id values to local records
            table.data.forEach((row) => {
                if (row[table._id]) {
                    row._id = row[table._id];
                }
            });


            // timeLog(`Mapping IDs for ${table.name} (${table._id})`);
            
            const parent = this;

            this.idMap[table.name] = { _missing: [], _duplicates: [] };
    
            parent.base(table.name).select({
                // pageSize: 1,
                fields: [table._id]
            }).eachPage(function page(records, fetchNextPage) {
                records.forEach((record, i) => {
                    const dbId = record.get(table._id);
                    const recordId = record.getId().toString().replace(/^\s+|\s+$/g, '');
                    if (dbId) {
                        if (parent.idMap[table.name][dbId]) {
                            parent.idMap[table.name]._duplicates.push(recordId);
                        } else {
                            parent.idMap[table.name][dbId] = recordId;
                        }
                    } else {
                        parent.idMap[table.name]._missing.push(recordId);
                    }
                });
                fetchNextPage();
            }, function done(err) {
                if (err) { reject(err); return; }
                // timeLog('done', idMap[table.name]);
                resolve();
            });
        });
    }

    async pushTableData() {
        for (let i = 0; i < this.tables.length; i += 1) {
            this.tables[i].data = await this.pushEachTableData(this.tables[i]);
        }
    }

    async pushEachTableData(table) {    
        const tableIdMap = this.idMap[table.name];
        if (!tableIdMap) return;
    
        const update = [];
        const create = [];
    
        table.data.forEach((record) => {
            // console.log(record, table.fields);
            const fields = _.pick(record, table.fields);
            // console.log(fields);
            const id = tableIdMap[record._id.toString().replace(/^\s+|\s+$/g, '')];
            if (id) {
                update.push({ id, fields });
            } else {
                create.push({ fields });
            }
    
            if (record._join) {
                _.forIn(record._join, (value, localField) => {
                    const join = {
                        localTable: table.name,
                        localId: record._id,
                        localField,
                        foreignTable: value.table,
                        foreignIds: value.ids
                    };
                    this.joins.push(join);
                });
            }
        });
    
        // console.log(update);
        // console.log(create);
        this.timeLog(table.name, { update: update.length, create: create.length });
    
        for (let i = 0; i < update.length; i += this.recordsPerSecond) {
            this.timeLog(`Updating ${table.name} records ${i} - ${i + this.recordsPerSecond} / ${update.length}`);
            await this.airtableRateLimit(
                this.base(table.name).update,
                update.slice(i, i + this.recordsPerSecond))
                .catch((error) => { throw error; }
            );
        }
        for (let i = 0; i < create.length; i += this.recordsPerSecond) {
            this.timeLog(`Creating ${table.name} records ${i} - ${i + this.recordsPerSecond} / ${create.length}`);
            const inserts = await this.airtableRateLimit(
                this.base(table.name).create,
                create.slice(i, i + this.recordsPerSecond))
                .catch((error) => { throw error; }
            );
    
            inserts.forEach((record) => {
                const dbId = record.get(table._id);
                if (dbId) {
                    this.idMap[table.name][dbId] = record.getId();
                } else {
                    // Airtable records with an empty _id field
                    this.idMap[table.name]._missing.push(record.getId());
                }
            });
        }
    
        this.timeLog('Done');
        this.log += '\n';
    
        // tableIdMap._missing // remove records with missing IDs from Airtable
    }

    async pushJoins() {
        this.joins.forEach((join) => {
            join.recordId = this.idMap[join.localTable][join.localId];
            if (join.recordId) {
                join.fieldValue = _.chain(join.foreignIds)
                    .map(foreignId => this.idMap[join.foreignTable] && this.idMap[join.foreignTable][foreignId])
                    .filter()
                    .value();
    
                // console.log('join.fieldValue', join.fieldValue);
            }
            if (!_.get(join, 'fieldValue.length')) {
                join.fieldValue = null
            }
        });
    
        // console.log('joins', joins);
    
        const tableJoins = _.chain(this.joins)
            .filter(join => join.recordId && join.fieldValue)
            .groupBy('localTable')
            .mapValues((values, table) => _.chain(values).groupBy('recordId').value())
            .value();
    
        for (const tableName in tableJoins) {
            const table = _.find(this.tables, { name: tableName });
            const records = _.chain(tableJoins[tableName])
                .mapValues((record, id) => ({
                    id,
                    fields: _.chain(record)
                        .groupBy('localField')
                        .mapValues('0.fieldValue')
                        .pick(table.fields)
                        .value()
                }))
                .values()
                .value();
    
            for (let i = 0; i < records.length; i += this.recordsPerSecond) {
                this.timeLog(`Updating ${table.name} records ${i} - ${i + this.recordsPerSecond} / ${records.length}`);
                await this.airtableRateLimit(
                    this.base(table.name).update,
                    records.slice(i, i + this.recordsPerSecond)
                )
                .catch((error) => { throw new DumpError(error); });
            }
        }
    }

    async dedupeTables() {
        for (let i = 0; i < this.tables.length; i += 1) {
            this.timeLog(`De-duping ${this.tables[i].name}`);
            await this.dedupeEachTable(this.tables[i]).catch((e) => {
                console.error(e);
            });
        }
    }

    async dedupeEachTable(table) {
        const dupes = _.get(this.idMap, `${table.name}._duplicates`) || [];
        if (!dupes.length) {
            return;
        }

        for (const chunk of _.chunk(dupes, 10)) {
            await this.base(table.name).destroy(chunk);
        }

        this.timeLog(`Deleted ${dupes.length} duplicate rows`);
    }

    async dump(options = {}) {
        this.timeLog('Mapping IDs...');
        await this.mapTableIds();
        if (options.dedupe) {
            this.timeLog('Deduping Airtable data...');
            await this.dedupeTables();
        }
        this.timeLog('Pushing data to Airtable...');
        await this.pushTableData();
        this.timeLog('Performing table joins...');
        await this.pushJoins();
    }
}

module.exports = AirtableDump;
